# Paper Requirements

## Huge requirement
x1 = faulty ∨ alarm ∨ default ∨ IO status

x2 = (faulty ∧ alarm) ∨ (alarm ∧ default) ∨ (default ∧ faulty) ∨ IO status

x3 = faulty ∧ alarm ∧ default

G(¬IO status ⇒ ( X(x1) ∧ XX(x2) ∧ XXX(x3 U IO status) ))
```
      x_1 = \textit{faulty} \vee alarm \vee default \vee \emph{IO status} \\
      x_2 = (\textit{faulty} \wedge alarm) \vee (alarm \wedge default) \vee (default \wedge \textit{faulty}) \vee \emph{IO status}\\
      x_3 = \textit{faulty} \wedge alarm \wedge default \\
      \mathcal{F} &= G(\neg \emph{IO status} \rightarrow \mathcal{X}(x_1) \wedge \mathcal{X}^2(x_2) \wedge \mathcal{X}^3 ((x_3) \;\mathcal{U} (\emph{IO status})) )
```

## ''Next'' requirement
G((missing IO) ⇒ ( XXX(faulty ∧ default ∧ alarm) ) U ¬missing IO)

```
    \mathcal{G} \left(
      \neg \mbox{\emph{IO status}} \rightarrow \left(
          \mathcal{X}^3\left(
            \textit{faulty} \wedge \textit{alarm} \wedge \textit{default}
          \right)
        \right)
        \mathcal{U}
          (\mbox{\emph{IO status})}
    \right)
```

## While requirement
G((missing IO ∧ faulty ∧ default ∧ alarm) ⇒ (missing IO ∧ faulty ∧ default ∧ alarm) U ¬missing IO)

```
    \mathcal{G} ( (\neg \mbox{\emph{IO status}} \land faulty \land alarm \land default) \rightarrow\\
    ( (\neg \mbox{\emph{IO status}} \land faulty \land alarm \land default) \;\mathcal{U}\; \mbox{\emph{IO status}}) )
```

Simplified version is to obtain a figure for the paper

## Tracepoints
```
signal_app:add_alarm( alarm_number=394500)
signal_app:ALARM(     alarm_comand=clearAll, alarm_number=0)

signal_app:IODATA(    status=MISSING,        board_A_index=[0,1,2])
signal_app:IODATA(    status=RECEIVED,       board_A_index=[0,1,2])

signal_app:ASPECT(    status=DEFAULT,        board_index_field=[0,1,2], aperture_index_field=[0-8])
signal_app:ASPECT(    status=NODEFAULT,      board_index_field=[0,1,2], aperture_index_field=[0-8])

signal_app:APERTURE(  status=FAULTY,         board_A_index=[0,1,2])
signal_app:APERTURE(  status=WORKING,        board_A_index_[0,1,2])
```
