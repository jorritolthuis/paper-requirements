* `requirement.json` Simplified requirement (to reduce the number of states and transitions)
* `nba.json` Generated positive NBA
* `neg-nba.json` Generated negated NBA, reformatted for easier reading
* `neg-nba.gv` Dot file generated from `neg-nba.json`
* `neg-nba-no-true.gv` (Manually) removed `true` labels, and ordered transitions and removed duplicates
* `neg-nba-boolean.gv` Modified version of `neg-nba-no-true.gv` with boolean formulas on the labels
* `neg-nba.svg` Graph of `neg-nba.gv`
* `neg-nba-no-true.svg` Graph of `neg-nba-no-true.gv`
* `neg-nba-boolean.svg` Graph of `neg-nba-boolean.gv`
* `neg-nba-boolean-xlabel.svg` Graph of `neg-nba-boolean.gv` with `xlabel=` rather than `label=` (https://www.graphviz.org/faq/#FaqEdgeLabelPlace)
* `neg-nba-boolean-xlabel.svg` Graph of `neg-nba-boolean-xlabel.svg` with `rankdir="LR";`
